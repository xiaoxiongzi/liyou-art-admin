const state = {
  clientHeight: document.documentElement.clientHeight || 1080,
};

const mutations = {
  SET_CLIENTHEIGHT(state, height) {
    state.clientHeight = height;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
};
