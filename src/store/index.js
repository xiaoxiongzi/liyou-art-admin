import Vue from 'vue';
import Vuex from 'vuex';
import user from './user';
import client from './client';

Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    user,
    client,
  },
});
