import { getUserInfo } from '@/api/user';

const state = {
  user: {},
  isLogin: false,
};

const actions = {
  async getUserInfo({ commit }) {
    const user = await getUserInfo();
    commit('SET_USER', user);
    return user;
  },
};

const mutations = {
  SET_USER(state, user) {
    state.user = user;
    state.isLogin = true;
  },
  CLEAR_LOGIN(state) {
    state.user = {};
    state.isLogin = false;
    localStorage.removeItem('token');
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
