export default [
  {
    label: '基本配置',
    name: 'base-config',
    children: [
      {
        label: '首页轮播图',
        name: 'carousel',
      },
      {
        label: '消息管理',
        name: 'message',
      },
      {
        label: '经验等级',
        name: 'experience-level',
      },
      {
        label: '提现手续费',
        name: 'withdraw-fee',
      },
      {
        label: '营销配置',
        name: 'marketing',
      },
      {
        label: '赠送年卡',
        name: 'giveVip',
      },
      {
        label: '赠送课程',
        name: 'giveCourse',
      },
    ],
  },
];
