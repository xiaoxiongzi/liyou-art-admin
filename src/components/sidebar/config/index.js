import order from './order';
import baseConfig from './base-config';
import course from './course';

export default [
  {
    label: '控制面板',
    name: 'dashboard',
  },
  {
    label: '账号管理',
    name: 'account',
    children: [
      {
        label: '账号列表',
        name: 'account-list',
      },
    ],
  },
  ...course,
  {
    label: '用户管理',
    name: 'user',
    children: [
      {
        label: '用户列表',
        name: 'user-list',
      },
    ],
  },
  {
    label: '作品管理',
    name: 'works',
    children: [
      {
        label: '作品列表',
        name: 'works-list',
      },
    ],
  },
  ...order,
  ...baseConfig,
];
