export default [
  {
    label: '课程管理',
    name: 'course',
    children: [
      {
        label: '学龄管理',
        name: 'school-age',
      },
      {
        label: '一级分类列表',
        name: 'first-class',
      },
      {
        label: '二级分类列表',
        name: 'second-class',
      },
      {
        label: '课程列表',
        name: 'course-list',
      },
      {
        label: '辅材列表',
        name: 'supplement',
      },
      {
        label: '积分礼品列表',
        name: 'points-gift',
      },
      {
        label: '评价列表',
        name: 'comment-list',
      },
      {
        label: '评论标签列表',
        name: 'comment-tag',
      },
    ],
  },
];
