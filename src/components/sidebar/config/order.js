export default [
  {
    label: '订单管理',
    name: 'order',
    children: [
      {
        label: '订单列表',
        name: 'order-list',
      },
      {
        label: '销售明细',
        name: 'distribution',
      },
      {
        label: '提现明细',
        name: 'withdraw',
      },
    ],
  },
];
