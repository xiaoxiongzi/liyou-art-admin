export const distribution = [
  {
    userName: '吴亦凡',
    income: '28',
    saleAmout: '售课1677元',
    dealAt: 1593077893,
  },
];

export const withdraw = [
  {
    userName: '吴彦祖',
    withdrawAmount: '28',
    saleAmout: '售课1677元',
    applyAt: 1593077893,
    status: 1,
  },
];

export const order = [
  {
    orderID: '12345678910',
    orderType: 1,
    course: '毕加索经典课程1',
    auxiliary: ['辅材名称1', '辅材名称2'],
    pointsGift: '礼品1名称',
    buyer: '吴彦祖',
    buyerPhone: '15521398273',
    address: '广州天河车陂骏景花园A座4',
    receivedAmount: '1280.50',
    liyouCard: 200,
    orderUser: '吴亦凡',
    orderAt: 1593077893,
    orderStatus: 2,
    trackingID: '32y89sdojon',
  },
];
