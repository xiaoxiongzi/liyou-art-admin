export const searchConfig = [
  {
    type: 'input',
    prop: 'name',
    label: '课程名称',
  },
  {
    type: 'select',
    prop: 'firstClass',
    label: '一级分类',
    options: [],
  },
  {
    type: 'select',
    prop: 'secondClass',
    label: '二级分类',
    options: [],
  },
  {
    type: 'select',
    prop: 'status',
    label: '状态',
    options: [],
  },
  {
    type: 'datetimerange',
    prop: 'timeRange',
    label: '入驻时间',
  },
];

export const columns = [
  { prop: 'orderID', label: '订单号', minWidth: '100px' },
  { prop: 'orderType', label: '订单类型', minWidth: '80px' },
  { prop: 'course', label: '课程', minWidth: '120px' },
  { prop: 'auxiliary', label: '辅材', minWidth: '140px' },
  { prop: 'pointsGift', label: '积分礼品', minWidth: '80px' },
  { prop: 'buyer', label: '买家/收货人', minWidth: '110px' },
  { prop: 'receivedAmount', label: '实收金额（元）', minWidth: '110px' },
  { prop: 'liyouCard', label: '消耗里由卡（张）', minWidth: '120px' },
  { prop: 'orderUser', label: '下单用户', minWidth: '100px' },
  { prop: 'orderAt', label: '下单时间', minWidth: '130px' },
  { prop: 'orderStatus', label: '订单状态', minWidth: '80px' },
  { prop: 'trackingID', label: '物流单号', minWidth: '120px' },
];

export const schemaMap = [
  { prop: 'orderID', label: '订单号' },
  { prop: 'orderType', label: '订单类型' },
  { prop: 'course', label: '课程' },
  { prop: 'auxiliary', label: '辅材' },
  { prop: 'pointsGift', label: '积分礼品' },
  { prop: 'buyer', label: '收货人' },
  { prop: 'buyerPhone', label: '手机号码' },
  { prop: 'address', label: '收货地址' },
  { prop: 'receivedAmount', label: '实收金额（元）' },
  { prop: 'liyouCard', label: '消耗里由卡（张）' },
  { prop: 'orderUser', label: '下单用户' },
  { prop: 'orderAt', label: '下单时间' },
  { prop: 'orderStatus', label: '订单状态' },
  { prop: 'trackingID', label: '物流单号' },
];
