import { ORDER_STATUS_OPTIONS, ORDER_TYPE_OPTIONS } from '@/utils/constants';

export const searchConfig = [
  {
    type: 'input',
    prop: 'orderNo',
    label: '订单号',
  },
  {
    type: 'datetimerange',
    prop: 'timeRange',
    label: '申请时间',
  },
  {
    type: 'input',
    prop: 'linkName',
    label: '收货人姓名',
  },
  {
    type: 'input',
    prop: 'phone',
    label: '买家手机号',
  },
  {
    type: 'input',
    prop: 'courseName',
    label: '课程名称',
  },
  {
    type: 'input',
    prop: 'courseMaterialName',
    label: '辅材名称',
  },
  {
    type: 'input',
    prop: 'pointsGift',
    label: '积分礼品名称',
  },
  {
    type: 'select',
    prop: 'orderStatus',
    label: '订单状态',
    options: ORDER_STATUS_OPTIONS,
  },
  {
    type: 'select',
    prop: 'orderType',
    label: '订单类型',
    options: ORDER_TYPE_OPTIONS,
  },
];

export const columns = [
  { prop: 'orderNo', label: '订单号', minWidth: '180px' },
  { prop: 'orderType', label: '订单类型', minWidth: '100px' },
  { prop: 'courseName', label: '课程', minWidth: '180px' },
  { prop: 'courseMaterialName', label: '辅材', minWidth: '180px' },
  { prop: 'scoreGiftName', label: '积分礼品', minWidth: '120px' },
  { prop: 'linkName', label: '买家/收货人', minWidth: '110px' },
  { prop: 'actualPrice', label: '实收金额（元）', minWidth: '140px' },
  { prop: 'scholarshipPrice', label: '消耗里由卡（张）', minWidth: '150px' },
  { prop: 'userName', label: '下单用户', minWidth: '140px' },
  { prop: 'createTime', label: '下单时间', minWidth: '140px' },
  { prop: 'orderStatus', label: '订单状态', minWidth: '80px' },
  { prop: 'shipNo', label: '物流单号', minWidth: '120px' },
];

export const schemaMap = [
  { prop: 'orderNo', label: '订单号' },
  { prop: 'orderType', label: '订单类型' },
  { prop: 'courseName', label: '课程' },
  { prop: 'courseMaterialName', label: '辅材' },
  { prop: 'scoreGiftName', label: '积分礼品' },
  { prop: 'linkName', label: '买家/收货人' },
  { prop: 'actualPrice', label: '实收金额（元）' },
  { prop: 'scholarshipPrice', label: '消耗里由卡（张）' },
  { prop: 'userName', label: '下单用户' },
  { prop: 'createTime', label: '下单时间' },
  { prop: 'phone', label: '手机号码' },
  { prop: 'address', label: '收货地址' },
  { prop: 'orderStatus', label: '订单状态' },
  { prop: 'shipNo', label: '物流单号' },
];
