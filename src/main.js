import Vue from 'vue';
import ElementUI from 'element-ui';
import VueQuillEditor from 'vue-quill-editor';
import plugins from '@/utils/plugins';
import prototype from '@/utils/prototype';
import App from './App';
import router from './router';
import store from './store';
import GlobalMixin from './globalMixin';
import 'element-ui/lib/theme-chalk/index.css';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
import '@/assets/main.scss';

Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(VueQuillEditor);
Vue.use(plugins);
Vue.use(prototype);
Vue.mixin(GlobalMixin);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
