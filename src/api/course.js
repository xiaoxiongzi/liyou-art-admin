import http from '@/utils/request';

/** 学龄管理 */
export function getStudentAgeList(params) {
  return http.post('/api/admin/studentAge/listPage', params);
}
export function saveStudentAge(params) {
  return http.post('/api/admin/studentAge/save', params);
}
export function deleteStudentAge(params) {
  return http.post('/api/admin/studentAge/delete', params);
}
export function updateStudentAge(params) {
  return http.post('/api/admin/studentAge/update', params);
}

/** 辅材列表 */
export function getSupplementList(params) {
  return http.post('/api/admin/courseMaterial/listPage', params);
}
export function saveSupplement(params) {
  return http.post('/api/admin/courseMaterial/save', params);
}
export function deleteSupplement(params) {
  return http.post('/api/admin/courseMaterial/delete', params);
}
export function updateSupplement(params) {
  return http.post('/api/admin/courseMaterial/update', params);
}

/** 评论标签列表 */
export function getCommentTagList(params) {
  return http.post('/api/admin/commentTag/listPage', params);
}
export function saveCommentTag(params) {
  return http.post('/api/admin/commentTag/save', params);
}
export function deleteCommentTag(params) {
  return http.post('/api/admin/commentTag/delete', params);
}
export function updateCommentTag(params) {
  return http.post('/api/admin/commentTag/update', params);
}
export function getCommentTagInfo(params) {
  return http.post('/api/admin/commentTag/info', params);
}

/** 评论列表 */
export function getCommentList(params) {
  return http.post('/api/admin/comment/listPage', params);
}
export function updateComment(params) {
  return http.post('/api/admin/comment/delete', params);
}
export function getCommentInfo(params) {
  return http.post('/api/admin/comment/info', params);
}

/** 一级分类 和 二级分类 */
export function getCategoryList(params) {
  return http.post('/api/admin/courseCategory/listPage', params);
}
export function saveCategory(params) {
  return http.post('/api/admin/courseCategory/save', params);
}
export function updateCategory(params) {
  return http.post('/api/admin/courseCategory/update', params);
}
export function getCategoryInfo(params) {
  return http.post('/api/admin/courseCategory/info', params);
}
export function deleteCategory(params) {
  return http.post('/api/admin/courseCategory/delete', params);
}

/** 积分礼品 */
export function getPointGiftList(params) {
  return http.post('/api/admin/scoreGift/listPage', params);
}
export function savePointGift(params) {
  return http.post('/api/admin/scoreGift/save', params);
}
export function updatePointGift(params) {
  return http.post('/api/admin/scoreGift/update', params);
}
export function deletePointGift(params) {
  return http.post('/api/admin/scoreGift/delete', params);
}

/** 课程列表 */
export function getCourseList(params) {
  return http.post('/api/admin/course/listPage', params);
}
export function saveCourse(params) {
  return http.post('/api/admin/course/save', params);
}
export function updateCourse(params) {
  return http.post('/api/admin/course/update', params);
}
export function deleteCourse(params) {
  return http.post('/api/admin/course/delete', params);
}
export function getCourseInfo(params) {
  return http.post('/api/admin/course/info', params);
}
