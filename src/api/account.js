import http from '@/utils/request';

/** adminUser */
export function getAdminUserList(params) {
  return http.post('/api/admin/adminUser/listPage', params);
}
export function saveAdminUser(params) {
  return http.post('/api/admin/adminUser/save', params);
}
export function updateAdminUser(params) {
  return http.post('/api/admin/adminUser/update', params);
}
export function updateAdminUserPwd(params) {
  return http.post('/api/admin/adminUser/updatePwd', params);
}
export function deleleAdminUser(params) {
  return http.post('/api/admin/adminUser/delete', params);
}

export function login(params) {
  return http.post('/api/admin/index/login', params);
}
