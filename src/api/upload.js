import http from '@/utils/request';

export function getVodToken() {
  return http.post('/api/admin/common/getVodToken');
}

export function getOssToken() {
  return http.post('/api/admin/common/getOssToken');
}

export function getVideoInfo(params) {
  return http.post('/api/admin/common/getVideoInfo', params);
}
