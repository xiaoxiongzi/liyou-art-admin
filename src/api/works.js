import http from '@/utils/request';

export function getWorkList(params) {
  return http.post('/api/admin/studentWorks/listPage', params);
}

export function modifyWorkThumbCount(params) {
  return http.post('/api/admin/studentWorks/update', params);
}
