import http from '@/utils/request';

export function getUserList(params) {
  return http.post('/api/admin/user/listPage', params);
}

export function getUserInfo(params) {
  return http.post('/api/user/login', params);
}
