import http from '@/utils/request';

export function getWithdrawList(params) {
  return http.post('/api/admin/withdrawRecord/listPage', params);
}

export function getOrderList(params) {
  return http.post('/api/admin/order/listPage', params);
}

export function getCommissionList(params) {
  return http.post('/api/admin/userCommissionRecord/listPage', params);
}
