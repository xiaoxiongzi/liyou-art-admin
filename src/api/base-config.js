import http from '@/utils/request';

/** 提现手续费 */
export function getWithdrawRuleList(params) {
  return http.post('/api/admin/withdrawRule/listPage', params);
}
export function getWithdrawInfo(params) {
  return http.post('/api/admin/withdrawRule/info', params);
}
export function saveWithdrawRule(params) {
  return http.post('/api/admin/withdrawRule/save', params);
}
export function deleteWithdrawRule(params) {
  return http.post('/api/admin/withdrawRule/delete', params);
}
export function updateWithdrawRule(params) {
  return http.post('/api/admin/withdrawRule/update', params);
}

/** 经验管理 */
export function getExperienceLevelList(params) {
  return http.post('/api/admin/experienceLevel/listPage', params);
}
export function saveExperienceLevel(params) {
  return http.post('/api/admin/experienceLevel/save', params);
}
export function deleteExperienceLevel(params) {
  return http.post('/api/admin/experienceLevel/delete', params);
}
export function updateExperienceLevel(params) {
  return http.post('/api/admin/experienceLevel/update', params);
}
export function getExperienceLevelInfo(params) {
  return http.post('/api/admin/experienceLevel/info', params);
}

/** 消息管理 */
export function getMessageList(params) {
  return http.post('/api/admin/message/listPage', params);
}
export function saveMessage(params) {
  return http.post('/api/admin/message/save', params);
}
export function deleteMessage(params) {
  return http.post('/api/admin/message/delete', params);
}
export function updateMessage(params) {
  return http.post('/api/admin/message/update', params);
}
export function getMessageInfo(params) {
  return http.post('/api/admin/message/info', params);
}

/** 首页轮播图 */
export function getBannerList(params) {
  return http.post('/api/admin/banner/listPage', params);
}
export function saveBanner(params) {
  return http.post('/api/admin/banner/save', params);
}
export function deleteBanner(params) {
  return http.post('/api/admin/banner/delete', params);
}
export function updateBanner(params) {
  return http.post('/api/admin/banner/update', params);
}
export function getBannerInfo(params) {
  return http.post('/api/admin/banner/info', params);
}

/** 营销配置 */
export function getSystemConfigList(params) {
  return http.post('/api/admin/systemConfig/listPage', params);
}
export function saveSystemConfig(params) {
  return http.post('/api/admin/systemConfig/saveOrUpdate', params);
}

/** 赠送年卡 */
export function giveVip(params) {
  return http.post('/api/admin/userVipRecord/give', params);
}

/** 赠送 */
export function giveCourse(params) {
  return http.post('/api/admin/course/give', params);
}
