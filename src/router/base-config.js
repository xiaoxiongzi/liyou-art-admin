export default {
  path: '/config',
  name: 'base-config',
  component: { render: (h) => h('router-view') },
  children: [
    {
      path: 'carousel',
      name: 'carousel',
      component: () => import('@/views/base-config/carousel'),
    },
    {
      path: 'experience-level',
      name: 'experience-level',
      component: () => import('@/views/base-config/experience-level'),
    },
    {
      path: 'message',
      name: 'message',
      component: () => import('@/views/base-config/message'),
    },
    {
      path: 'withdraw-fee',
      name: 'withdraw-fee',
      component: () => import('@/views/base-config/withdraw-fee'),
    },
    {
      path: 'marketing',
      name: 'marketing',
      component: () => import('@/views/base-config/marketing'),
    },
    {
      path: 'giveVip',
      name: 'giveVip',
      component: () => import('@/views/base-config/giveVip'),
    },
    {
      path: 'giveCourse',
      name: 'giveCourse',
      component: () => import('@/views/base-config/giveCourse'),
    },
  ],
};
