export default {
  path: '/order',
  name: 'order',
  component: { render: (h) => h('router-view') },
  children: [
    {
      path: 'list',
      name: 'order-list',
      component: () => import('@/views/order/list'),
    },
    {
      path: 'distribution',
      name: 'distribution',
      component: () => import('@/views/order/distribution'),
    },
    {
      path: 'withdraw',
      name: 'withdraw',
      component: () => import('@/views/order/withdraw'),
    },
  ],
};
