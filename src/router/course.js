export default {
  path: '/course',
  name: 'course',
  component: { render: (h) => h('router-view') },
  children: [
    {
      path: 'school-age',
      name: 'school-age',
      component: () => import('@/views/course/school-age'),
    },
    {
      path: 'first-class',
      name: 'first-class',
      component: () => import('@/views/course/first-class'),
    },
    {
      path: 'second-class',
      name: 'second-class',
      component: () => import('@/views/course/second-class'),
    },
    {
      path: 'course-list',
      name: 'course-list',
      component: () => import('@/views/course/course-list'),
    },
    {
      path: 'comment-list',
      name: 'comment-list',
      component: () => import('@/views/course/comment-list'),
    },
    {
      path: 'comment-tag',
      name: 'comment-tag',
      component: () => import('@/views/course/comment-tag'),
    },
    {
      path: 'supplement',
      name: 'supplement',
      component: () => import('@/views/course/supplement'),
    },
    {
      path: 'points-gift',
      name: 'points-gift',
      component: () => import('@/views/course/points-gift'),
    },
    {
      path: 'course-edit',
      name: 'course-edit',
      component: () => import('@/views/course/course-list/edit'),
      meta: {
        menuItem: 'course-list',
      },
    },
  ],
};
