export default {
  data() {
    return {
      pageSize: 10,
      pageNum: 1,
      total: 0,
    };
  },
  computed: {
    searchParams() {
      return {
        ...(this.searchModel ? this.searchModel : {}),
        pageSize: this.pageSize,
        pageNum: this.pageNum - 1,
      };
    },
  },
  methods: {
    handleSizeChange(val) {
      this.pageSize = val;
    },
    async handleCurrentChange(val) {
      this.pageNum = val;
      this.getData();
    },
  },
};
