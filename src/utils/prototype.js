import dayjs from 'dayjs';

export default {
  install(Vue) {
    Vue.prototype.$formatTime = (unixTime) => (unixTime ? dayjs(unixTime).format('YYYY.MM.DD HH:mm') : '');
  },
};
