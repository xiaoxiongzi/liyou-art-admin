const isProd = process.env.NODE_ENV === 'production';
export const API_HOST = isProd ? 'http://121.196.188.137:8080' : '/';

// 角色类型
export const ROLE_TYPE = {
  1: '超级管理员',
  2: '教务主管',
  3: '财务主管',
};
// 角色类型选项
export const ROLE_TYPE_OPTIONS = [
  { value: 1, label: '超级管理员' },
  { value: 2, label: '教务主管' },
  { value: 3, label: '财务主管' },
];

// 订单类型
export const ORDER_TYPE = {
  1: '课程订单',
  2: '辅材订单',
  3: '积分礼品订单',
};
// 订单类型选项
export const ORDER_TYPE_OPTIONS = [
  { value: '', label: '全部' },
  { value: 1, label: '课程订单' },
  { value: 2, label: '辅材订单' },
  { value: 3, label: '积分礼品订单' },
];

// 订单状态选项
export const ORDER_STATUS_OPTIONS = [
  { value: '', label: '全部' },
  { value: 101, label: '待付款' },
  { value: 201, label: '待发货' },
  { value: 301, label: '待收货' },
  { value: 401, label: '已完成' },
  { value: 402, label: '已关闭' },
  { value: 102, label: '已取消' },
];

export const COMMENT_STATUS = {
  1: '已隐藏',
  2: '正常',
};

export const COMMENT_STATUS_OPTIONS = [
  { value: 0, label: '全部' },
  { value: 1, label: '已隐藏' },
  { value: 2, label: '正常' },
];

export const ORDER_STATUS_MAP = {
  101: '待付款',
  102: '已取消', // 下单未支付用户取消
  103: '已取消', // 下单未支付超时系统自动取消
  201: '待发货',
  301: '待收货',
  401: '已完成', // 用户确认收货
  402: '已完成', // 系统自动收货
};
