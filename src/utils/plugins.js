import pageHeader from '@/components/page-layout/page-header';
import pageSearch from '@/components/page-layout/page-search';
import pagePagination from '@/components/page-layout/page-pagination';
import dialogFooter from '@/components/dialog-footer';
import actionText from '@/components/action-text';
import uploader from '@/components/uploader';
import uploadList from '@/components/upload-list';
import RichText from '@/components/rich-text';

export default {
  install(Vue) {
    Vue.component('page-search', pageSearch);
    Vue.component('page-header', pageHeader);
    Vue.component('page-pagination', pagePagination);
    Vue.component('dialog-footer', dialogFooter);
    Vue.component('action-text', actionText);
    Vue.component('uploader', uploader);
    Vue.component('uploadList', uploadList);
    Vue.component('RichText', RichText);
  },
};
