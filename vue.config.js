module.exports = {
  lintOnSave: false,
  configureWebpack: {
    devServer: {
      proxy: {
        '/api': {
          target: 'http://121.196.188.137:8080',
          changeOrigin: true,
        },
      },
    },
    resolve: {
      extensions: ['.js', '.vue', '.json'],
    },
  },
  chainWebpack: (config) => {
    config.optimization.minimize(false);
  },
};
