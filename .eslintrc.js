module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  globals: {
    'AliyunUpload': false,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-expressions': 0,
    "no-param-reassign": [2, { props: false }],
    'max-len': 0,
    'no-shadow': 0,
    'no-underscore-dangle': 0,
    'import/extensions': ['error', 'always', {
      'js': 'never',
      'vue': 'never'
    }],
    'import/prefer-default-export': 0,
    'prefer-template': 0,
    'object-shorthand': 0,
    'no-plusplus': 0,
  },
  'settings': {
    "import/resolver": {
      "webpack": {
        "config": "node_modules/@vue/cli-service/webpack.config.js"
      }
    }
  }
};
